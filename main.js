require('colors');
require('./utils/array_sample.js');

const fs         						   = require('fs');
const delay      						   = require('./delay.js');
const qrcode     						   = require('qrcode-terminal');
const { Client, MessageMedia, LocalAuth }  = require('whatsapp-web.js');

const AudioEngine = require('./audio.js');

const SESSION_FILE_PATH = './session.json';
let sessionCfg;
if (fs.existsSync(SESSION_FILE_PATH)) 
{
    sessionCfg = require(SESSION_FILE_PATH);
}

const client     = new Client({ puppeteer: { headless: true }, authStrategy: new LocalAuth() });

client.on('authenticated', (session) => 
{	
    sessionCfg = session;
	/*fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), (err) =>
	{
		if (!err)
			return;

       	console.error(err);
    });*/
});

const CONFIG   = require('./config.js');
const MESSAGES = require('./messages.js'); 
const CONTACTS = require('./contacts.js'); 

client.on('qr', (qr) => 
{
	qrcode.generate(qr, {small: true});
});

client.on('ready', () => 
{
    console.log('Conectado'.magenta);
});

client.on('message', async (msg) => 
{
	// ignora mensagens em grupo 
	if (msg.from.indexOf('@g.us') != -1)
		return;
	
	// procura se o número está cadastrado 
	let current_pair = null;
	let who_sended   = null;

	for(const pair of CONTACTS.pairs)
	{
		if (msg.from.indexOf(pair.numbers[0]) != -1)
		{
			current_pair = pair;
			who_sended   = 0;

			break;
		}

		if (msg.from.indexOf(pair.numbers[1]) != -1)
		{
			current_pair = pair;
			who_sended   = 1;

			break;
		}
	}

	if (current_pair == null)
		return;

	// controlador de mensagens automáticas ---------------------------
	current_pair.reset_timeout();

	// a outra pessoa é o reverso do atual ---------------------------- 
	const other          = 1 - who_sended;
	const other_number   = current_pair.numbers[other]      + '@c.us';
	const current_number = current_pair.numbers[who_sended] + '@c.us';

	await delay(CONFIG.MESSAGE_DELAY, true);

	if (who_sended == 0)
		console.log(current_pair.names[0][0].cyan + ': ');
	else
		console.log(current_pair.names[1][0].yellow +  ': ');

	if (msg.type == 'chat')
	{	
		// sanitiza os nomes -------------------------------------------------------------------------
		if (msg.body.indexOf('EPIC') != -1)
			msg.reply('OMG');

		for (const proxy_name of CONFIG.PROXY_NAMES)
		{
			msg.body = msg.body.replace(new RegExp('\\b' + proxy_name + '\\b', 'gi'), current_pair.names[other].sample());
		}

		for (const person_name of current_pair.names[who_sended])
		{
			msg.body = msg.body.replace(new RegExp('\\b' + person_name + '\\b', 'gi'), CONFIG.PROXY_NAMES.sample());
		}
		// -------------------------------------------------------------------------------------------

		if (!msg.isForwarded || CONFIG.ALLOW_FORWARED)
		{
			client.sendMessage(other_number, msg.body);
		}
		else
		{
			console.log('\t' + 'Encaminou mensagem. Respondendo com emoji');
			client.sendMessage(current_number, MESSAGES.emojis.sample());
			client.sendMessage(other_number,   MESSAGES.emojis.sample());
		}

		current_pair.new_message(who_sended, msg.body);

		console.log('\t' + msg.body.green);

		if (Math.random() <= CONFIG.AUDIO_PROBABILITY)
		{
			const audio = AudioEngine.analyze_text(current_pair.chat_compound.join(' '));

			if (!audio)
				return;

			client.sendMessage(msg.from, MessageMedia.fromFilePath('audios/' + audio), {sendAudioAsVoice: false});
			console.log('System:'.magenta);
			console.log('\t' + 'Respondendo com áudio automático');			
		}

		return;
	}

	if (msg.type == 'ptt')
	{
		if (!CONFIG.ALLOW_PTT)
		{
			client.sendMessage(msg.from,  MESSAGES.no_audio.concat(MESSAGES.emojis).sample());

			console.log('\t' + 'Enviou ptt - enviando resposta automática');	
			return;
		}

		client.sendMessage(other_number, await msg.downloadMedia(), {sendAudioAsVoice: true});

		console.log('\t' + 'Enviou ptt');

		return;
	}

	if (msg.type == 'sticker')
	{
		if (!CONFIG.ALLOW_STICKER)
			return;

		if (!msg.hasMedia)
			return;

		client.sendMessage(other_number, await msg.downloadMedia());

		console.log('\t' + 'Enviou sticker');

		return;
	}

	if (msg.type == 'image')
	{
		if (!CONFIG.ALLOW_IMAGE)
			return;

		if (!msg.hasMedia)
			return;

		client.sendMessage(other_number, await msg.downloadMedia());
		
		console.log('\t' + 'Enviou imagem');
		
		return;
	}
	
	if (msg.type == 'audio')
	{
		client.sendMessage(current_number, MESSAGES.no_audio.concat(MESSAGES.emojis).sample());
		console.log('\t' + 'Enviou audio - enviando resposta automática');	

		return;
	}

});

function contact_pair()
{
	this.timeout         = null;
	this.timeout_counter = 0;

	this.chat_history  = [[], []];
	this.chat_compound = [];

	this.reset_timeout = function()
	{
		this.timeout_counter = 0;
		clearTimeout(this.timeout);

		this.new_timeout();
	}

	this.new_timeout = function()
	{
		if (++this.timeout_counter > CONFIG.TIMEOUT_RETRIES)
			return;

		this.timeout = setTimeout(() =>
		{

			const send_awser_if_question = (user) =>
			{
				if (this.timeout_counter != 1)
					return false;

				if (this.chat_history[user].length == 0)
					return false;

				if (this.chat_history[user][this.chat_history[user].length - 1].indexOf('?') == -1)
					return false;

				const awser_to_send = MESSAGES.awsers.sample();

				client.sendMessage(this.numbers[user] + '@c.us', awser_to_send);

				console.log('System:'.magenta);
				console.log('\t' + 'Respondendo por timeout: ' + awser_to_send.green);

				return true;
			}

			const message_to_send = MESSAGES.phrases.concat(MESSAGES.emojis).sample();

			if (!send_awser_if_question(0))
				client.sendMessage(this.numbers[0] + '@c.us', message_to_send);

			if (!send_awser_if_question(1))
				client.sendMessage(this.numbers[1] + '@c.us', message_to_send);

			console.log('System:'.magenta);
			console.log('\t' + 'Enviando por timeout: ' + message_to_send.green);
			
			this.new_timeout();
		}, this.timeout_counter**3 * CONFIG.TIMEOUT_DELAY);
	}

	this.new_message = function(who_sended, message_text)
	{
		message_text = message_text.trim();
		
		if (message_text == '')
			return;

		this.chat_history[who_sended].push(message_text);

		this.chat_compound.push(message_text);

		while (this.chat_compound.length > CONFIG.CHAT_HISTORY_LENGTH) 
			this.chat_compound.shift();

		while (this.chat_history[who_sended].length > CONFIG.CHAT_HISTORY_LENGTH) 
			this.chat_history[who_sended].shift();
	}
}

CONTACTS.pairs = CONTACTS.pairs.map((a) => Object.assign(new contact_pair(), a));

client.initialize();