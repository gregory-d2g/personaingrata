module.exports = (timeout, jitter = false) =>
{
	return new Promise((resolve) =>
	{
		if (jitter)
			timeout *= 0.9 + 0.2 * Math.random();
		
		setTimeout(resolve, timeout);
	})
}