const config = {};

config.PROXY_NAMES      = ['Gregory',			// lista de nomes que serão substituídos pelo nome do destinatário, case insensitivo
						   'Grégory',
						   'greg', 'gatinho', 'gregori'];

config.MESSAGE_DELAY   = 3500;					// tempo de atraso para enviar mensagens 

// flags de autorizacao de tipos de mensagens -----------------------------------------------------------------------------------------------
config.ALLOW_PTT      = false;					// caso false, irá envier config.MESSAGE_NO_PTT como resposta
config.ALLOW_IMAGE    = false;
config.ALLOW_STICKER  = false;
config.ALLOW_FORWARED = false;					// autoriza o envio de mensagens encaminhadas 

// parametros do envio automatico de mensagens ----------------------------------------------------------------------------------------------
config.TIMEOUT_DELAY   = 5 * 60 * 1000; 		// tempo de espera para enviar mensagem aleatoria, elevado ao cubo a cada mensagem
config.TIMEOUT_RETRIES = 3;					 	// até quantas mensagens aleatorias serão mandadas

// parametros da sistema de audios automaticos ----------------------------------------------------------------------------------------------
config.AUDIO_PROBABILITY   = 1/30;				// chance de um áudio ser enviado como resposta
config.AUDIO_MIN_SCORE     = 4;
config.CHAT_HISTORY_LENGTH = 12;

module.exports = config;