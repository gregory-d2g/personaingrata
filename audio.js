const audio_db = require('./audio_db.js');
const CONFIG   = require('./config.js');

class AudioEngine
{
	static analyze_text(text)
	{
		const computed_db = new Array(audio_db.length);

		text = AudioEngine.__normalize(text);

		for (let i = 0; i < computed_db.length; i++)
		{
			computed_db[i] = {audio_db: audio_db[i], score: AudioEngine.__score(text, audio_db[i].tags)};
		}

		computed_db.sort((a, b) =>
		{
			return b.score - a.score;
		});

		//console.log(computed_db);

		if (computed_db[0].score < CONFIG.AUDIO_MIN_SCORE)
			return null;

		return computed_db[0].audio_db.file;
	}

	static __score(input_text, tags)
	{
		//console.log('ANALISANDO: ', input_text);
		//console.log('\t', 'TAGS: ', tags);

		input_text = input_text.trim();

		if (input_text == '')
			return null;
		
		let score         = 0;
		let score_counter = 0;

		const input_words = input_text.split(' ');

		for (let tp = 0; tp < tags.length; tp++)
		{
			const tag_phrase = tags[tp];
			const tag_words  = AudioEngine.__split_non_alphanumeric(tag_phrase);

			for (const tag_word of tag_words)
			{
				if (tag_word.length < 3) 
					continue;

				for (let i = 0; i < input_words.length; i++)
				{
					const input_word = input_words[i];

					if (input_word != tag_word)
						continue;

					const tag_score = (0.2 + i / input_words.length) * tag_word.length * 1 / (tp + 1);

					//console.log('\t', 'PONTUANDO: ', tag_word, '     ', tag_score);
					score += tag_score;
				}
				//score_counter++;
			}
		}

		//console.log();
		//console.log();
		
		//if (score_counter == 0)
		//	return 0;

		return score * (0.95 + 0.1 * Math.random());
	}

	static __split_non_alphanumeric(text)
	{
		return text.split(/[^a-zA-Z\d]/g);
	}

	static __normalize(text)
	{
		return text.normalize("NFD").replace(/[\u0300-\u036f]/gi, "").toLowerCase();
	}
}


module.exports = AudioEngine;