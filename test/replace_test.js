require('colors');
const config = {};

config.PROXY_NAMES      = ['Samanta Holanda',			
						   'Samantha',
						   'Samantinha'];

// repetir para adicionar mais pares de pessoas
config.PERSON_NAMES      = ['Greg',			
						   'Gregory',
						   'Gusberti'];

		// sanitiza os nomes -------------------------------------------------------------------------
		
		nome = 'Oi Gregory';
		console.log('\t' + nome.yellow);
		
		for (const proxy_name of config.PROXY_NAMES)
		{
			nome = nome.replace(new RegExp( '\b' + proxy_name + '\b', 'gi'), config.PERSON_NAMES[0]);
		}
		for (const person_name of config.PERSON_NAMES)
		{
			nome = nome.replace(new RegExp('\b' + person_name + '\b', 'gi'), config.PROXY_NAMES[0]);
		}
		console.log('\t' + nome.green);

		// -------------------------------------------------------------------------------------------